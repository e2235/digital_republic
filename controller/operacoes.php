<?php 
const PORTA = 1.52;
const JANELA = 2.4;
function calculaM2($altura, $largura, $janela, $porta){  
    $areaParede = $altura * $largura;
    $areaPortaJanela = $janela * JANELA + $porta * PORTA;
    $areaTotal = $areaParede - $areaPortaJanela;

    if($porta == 0 || $altura >= 2.20){
        if($areaParede < 1 || $areaParede > 15){
            return "M2 da parede não está dentro do limite permitido!";
        }else{
            if($areaPortaJanela > ($areaParede/2)){
                return "M2 das Portas e Janelas excede o valor permitido!";
            }else{
                return $areaTotal;
            }
        }
    }else{
        return "Altura da parede não permite que se tenha uma porta!";
    }
}

function mts2Total($valor1, $valor2, $valor3, $valor4){
    if(!is_numeric($valor1) || !is_numeric($valor2) || !is_numeric($valor3) || !is_numeric($valor4)){
        return "Confira as medidas e refaça o cálculo por favor!";
    }else{
        return $valor1 + $valor2 + $valor3 + $valor4;
    }
}

function calculaLtsTinta($mts2){
    if(is_numeric($mts2)){
        $lts = $mts2/5;
        return  number_format($lts, 2, '.', ' ');
    }else{
        return "Não foi possível calcular a litragem, favor conferir os dados e refazer a operação!";
    }
    
}

function escolherLatas($lts){
    if(is_numeric($lts)){
        if($lts > 0 && $lts < 0.5){
            return "1 lata de 0.5L";
        }else if($lts > 0.5 && $lts <= 1){
            return "2 latas de 0.5L";
        }else if($lts > 1 && $lts <= 1.5){
            return "3 latas de 0.5L";
        }else if($lts > 1.5 && $lts <= 2){
            return "4 latas de 0.5L"; 
        }else if($lts > 2 && $lts <= 2.5){
            return "1 lata de 2.5L";
        }else if($lts > 2.5 && $lts <= 3){
            return "1 lata de 2.5L + 1 lata de 0.5L";
        }else if($lts > 3 && $lts <= 3.6){
            return "1 lata de 3.6L";
        }else if($lts > 3.6 && $lts <= 4.1){
            return "1 lata de 3.6L + 1 lata de 0.5L";
        }else if($lts > 4.1 && $lts <= 4.6){
            return "1 lata de 3.6L + 2 latas de 0.5L";
        }else if($lts > 4.6 && $lts <= 5.1){
            return "1 lata de 3.6L + 3 latas de 0.5L";
        }else if($lts > 5.1 && $lts <= 5.6){
            return "1 lata de 3.6L + 4 latas de 0.5L";
        }else if($lts > 5.6 && $lts <= 6.1){
            return "1 lata de 3.6L + 1 lata de 2.5L";
        }else if($lts > 6.1 && $lts <= 6.6){
            return "1 lata de 3.6L + 1 lata de 2.5L + 1 lata de 0.5L";
        }else if($lts > 6.6 && $lts <= 7.1){
            return "1 lata de 3.6L + 1 lata de 2.5L + 2 latas de 0.5L";
        }else if($lts > 7.1 && $lts <= 7.6){
            return "1 lata de 3.6L + 1 lata de 2.5L + 3 latas de 0.5L";
        }else if($lts > 7.6 && $lts <= 8.1){
            return "1 lata de 3.6L + 1 lata de 2.5L + 4 latas de 0.5L";
        }else if($lts > 8.1 && $lts <= 8.7){
            return "2 latas de 3.6L + 3 latas de 0.5L";
        }else if($lts > 8.7 && $lts <= 9.2){
            return "2 latas de 3.6L + 4 latas de 0.5L";
        }else if($lts > 9.2 && $lts <= 9.7){
            return "2 latas de 3.6L + 1 lata de 2.5L";
        }else if($lts > 9.7 && $lts <= 10.2){
            return "2 latas de 3.6L + 1 latas de 2.5L + 1 lata de 0.5L";
        }else if($lts > 10.2 && $lts <= 10.8){
            return "3 latas de 3.6L";
        }else if($lts > 10.7 && $lts <= 11.3){
            return "3 latas de 3.6L + 1 lata de 0.5L";
        }else if($lts > 11.3 && $lts <= 11.8){
            return "3 latas de 3.6L + 2 latas de 0.5L";
        }else if($lts > 11.8 && $lts <= 12.3){
            return "3 latas de 3.6L + 3 latas de 0.5L";
        }
    }else{
        return "Erro ao calcular!";
    }  
}
?>