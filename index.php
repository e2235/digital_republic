<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <title>Digital Cores</title>
    </head>
    <body>
        <main>
            <div class="row">
                <div class="col" id="cab">Digital Cores</div>
            </div>
            <div id="divCaixa" class="row">
                <div class="col-sm-6" id="form">
                    <form action="" method="POST">
                        <table id="tblpar">
                            <tr>
                                <td>
                                    <div>
                                        <label name="altura1" class="form-label">Parede 1</label>
                                        <input type="text" class="form-control" name="altura1parede" id="altura1parede" placeholder="Altura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <input type="text" class="form-control" name="largura1parede" id="largura1parede" placeholder="Largura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <label name="janela1" class="form-label" >Janela</label>
                                        <input type="number" class="form-control" min="0" value="0" name="janela1parede" id="janela1parede">
                                        <label name="porta1" class="form-label">Porta</label>
                                        <input type="number" class="form-control" min="0" value="0" name="porta1parede" id="porta1parede">
                                    </div>                                    
                                </td>
                                <td>
                                    <div>
                                        <label name="altura2" class="form-label">Parede 2</label>
                                        <input type="text" class="form-control" name="altura2parede" id="altura2parede" placeholder="Altura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <input type="text" class="form-control" name="largura2parede" id="largura2parede" placeholder="Largura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <label name="janela2" class="form-label" >Janela</label>
                                        <input type="number" class="form-control" min="0" value="0" name="janela2parede" id="janela2parede">
                                        <label name="porta2" class="form-label">Porta</label>
                                        <input type="number" class="form-control" min="0" value="0" name="porta2parede" id="porta2parede">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div>
                                        <label name="altura3" class="form-label">Parede 3</label>
                                        <input type="text" class="form-control" name="altura3parede" id="altura3parede" placeholder="Altura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <input type="text" class="form-control" name="largura3parede" id="largura3parede" placeholder="Largura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <label name="janela3" class="form-label" >Janela</label>
                                        <input type="number" class="form-control" min="0" value="0" name="janela3parede" id="janela3parede">
                                        <label name="porta3" class="form-label">Porta</label>
                                        <input type="number" class="form-control" min="0" value="0" name="porta3parede" id="porta3parede">
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <label name="altura4" class="form-label">Parede 4</label>
                                        <input type="text" class="form-control" name="altura4parede" id="altura4parede" placeholder="Altura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <input type="text" class="form-control" name="largura4parede" id="largura4parede" placeholder="Largura" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
                                        <label name="janela4" class="form-label" >Janela</label>
                                        <input type="number" class="form-control" min="0" value="0" name="janela4parede" id="janela4parede">
                                        <label name="porta4" class="form-label">Porta</label>
                                        <input type="number" class="form-control" min="0" value="0" name="porta4parede" id="porta4parede">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div id="btn">
                            <button type="submit" class="btn btn-primary" value="enviar">Calcular</button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6" id="oculta" style="display: none;">
                    <div class="oc" id="parede1"></div>
                    <div class="oc" id="parede2"></div>
                    <div class="oc" id="parede3"></div>
                    <div class="oc" id="parede4"></div>
                    <div class="oc" id="m2Total"></div>
                    <div class="oc" id="ltsTinta"></div>
                    <div class="oc" id="latas"></div>
                </div>
            </div>
        </main>            
    </body>
</html>
<?php
require("controller/operacoes.php");
if(isset($_POST['altura1parede']) && !empty($_POST['altura1parede'])){
    $primeiraParede = calculaM2($_POST['altura1parede'],$_POST['largura1parede'],$_POST['janela1parede'],$_POST['porta1parede']);
    $segundaParede = calculaM2($_POST['altura2parede'],$_POST['largura2parede'],$_POST['janela2parede'],$_POST['porta2parede']);
    $terceiraParede = calculaM2($_POST['altura3parede'],$_POST['largura3parede'],$_POST['janela3parede'],$_POST['porta3parede']);
    $quartaParede = calculaM2($_POST['altura4parede'],$_POST['largura4parede'],$_POST['janela4parede'],$_POST['porta4parede']);
    $areaTotal = mts2Total($primeiraParede , $segundaParede , $terceiraParede , $quartaParede);
    $litros = calculaLtsTinta($areaTotal);
    $latas = escolherLatas($litros);

    if(is_numeric($primeiraParede)){
        $parede1 = "M² da primeira parede: ".$primeiraParede;
    }else{
        $parede1 = $primeiraParede;
    }

    if(is_numeric($segundaParede)){
        $parede2 = "M² da segunda parede: ".$segundaParede;
    }else{
        $parede2 = $segundaParede;
    }

    if(is_numeric($terceiraParede)){
        $parede3 = "M² da terceira parede: ".$terceiraParede;
    }else{
        $parede3 = $terceiraParede;
    }

    if(is_numeric($quartaParede)){
        $parede4 = "M² da quarta parede: ".$quartaParede;
    }else{
        $parede4 = $quartaParede;
    }

    if(is_numeric($areaTotal)){
        $areaTot = "M² total: ".$areaTotal;
    }else{
        $areaTot = $areaTotal;
    }

    if(is_numeric($litros)){
        $litrosTot = "Será necessário ".$litros." L de tinta para pintar todo o cômodo!";
    }else{
        $litrosTot = $litros;
    }

    if(is_numeric($latas)){
        $latasTot = "Recomendamos: ".$latas;
    }else{
        $latasTot = $latas;
    }
    ?>
    <script type="text/javaScript">
        $('#oculta').show();
        $('#parede1').html("<label class='oc'><?=$parede1?></label>");
        $('#parede2').html("<label class='oc'><?=$parede2?></label>");
        $('#parede3').html("<label class='oc'><?=$parede3?></label>");
        $('#parede4').html("<label class='oc'><?=$parede4?></label>");
        $('#m2Total').html("<label class='oc'><?=$areaTot?></label>");
        $('#ltsTinta').html("<label class='oc'><?=$litrosTot?></label>");
        $('#latas').html("<label class='oc'><?=$latasTot?></label>");
    </script>
  <?php }
?>