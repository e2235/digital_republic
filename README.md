
# Projeto Digital Republic

### Pré-requisitos:

* Antes de começar, vamos precisar instalar o xampp em nossa máquina.

* Clique [AQUI](https://www.apachefriends.org/pt_br/index.html), escolha seu sistema operacional e faça o download.

* Após feita a instalação, abra o mesmo e clique em **start** para iniciar o Apache e o MySql.

* Precisaremos também do [Git](https://git-scm.com/downloads), escolha seu sistema operacional e faça o download.

### Executando a aplicação:

* Feita a instalção padrão do xampp, você deverá acessar a pasta htdocs (caminho comum C:\xampp\htdocs),
* Crie uma pasta com o nome de sua preferência (grave esse nome pois iremos precisar).
* Acesse a pasta recem criada, clique com o botão direito do mouse e inicie o gitbash que foi instalado juntamente com o Git.
* Após iniciado, use o comando **git init**, e **git clone <https://gitlab.com/e2235/digital_republic>**, certificado de que não ocorreu nenhum erro, vá ao seu navegador de preferência e digite localhost/"nome da sua pasta".
* Agora pode calcular quantos litros de tinta será necessário para pintar o seu cômodo.

Escolha sua cor favorita e alegre o seu lar.

